package main

import (
	"fmt"
)

func trip(s string) string {
	l := len(s)
	if l < 2 {
		return s
	}
	i := 0
	j := 1
	for j < l {
		if s[i] == s[j] {
			j++
		} else {
			i++
			s[i] = s[j]
		}
	}
	return s[:i]
}
func main() {
	s := "aabffhxyz"
	fmt.Println(trip(s))
}
