package main

import "fmt"

func test(list []int, target int) [][]int {
	i := 0
	j := len(list) - 1
	rets := [][]int{}
	for i < j {
		tmp := list[i] + list[j]
		if tmp == target {
			rets = append(rets, []int{i, j})
			i++
		} else if tmp < target {
			i++
		} else {
			j--
		}
	}
	return rets
}

func main() {
	list := []int{1, 2, 3, 4, 5, 6, 7}
	target := 6
	answers := test(list, target)
	for _, answer := range answers {
		fmt.Println(answer)
	}
}
