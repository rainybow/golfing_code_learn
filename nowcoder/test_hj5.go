/*
描述
写出一个程序，接受一个十六进制的数，输出该数值的十进制表示。

数据范围：保证结果在

注意本题有多组输入
输入描述：
输入一个十六进制的数值字符串。注意：一个用例会同时有多组输入数据，请参考帖子https://www.nowcoder.com/discuss/276处理多组输入的问题。

输出描述：
输出该数值的十进制字符串。不同组的测试用例用\n隔开。

示例1
输入：
0xA
0xAA
复制
输出：
10
170
*/
package main

import "fmt"

var hexMap = map[byte]int{
	'A': 10,
	'B': 11,
	'C': 12,
	'D': 13,
	'E': 14,
	'F': 15,
	'0': 0,
	'1': 1,
	'2': 2,
	'3': 3,
	'4': 4,
	'5': 5,
	'6': 6,
	'7': 7,
	'8': 8,
	'9': 9,
	'a': 10,
	'b': 11,
	'c': 12,
	'd': 13,
	'e': 14,
	'f': 15,
}

func main() {
	for {
		var s string
		if _, err := fmt.Scanf("%s", &s); err != nil {
			return
		}
		length := len(s)
		if length > 2 {
			num := 0
			base := 1
			for i := length - 1; i > 1; i-- {
				num = num + hexMap[s[i]]*base
				base = base * 16
			}
			fmt.Println(num)
		}
	}
}
