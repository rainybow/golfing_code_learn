package main

import (
	"fmt"
)

type Void struct{}

var void Void

func GetInputNumbers() (nums [][]int) {
	nums = make([][]int, 0)
	for {
		var n int
		if _, err := fmt.Scanf("%d", &n); err != nil {
			//	fmt.Printf("Debug:err:%v\n", err)
			break
		}
		//fmt.Printf("Debug:n:%v\n", n)
		if n == 0 {
			break
		}
		num := make([]int, n, n)
		for i := 0; i < n; i++ {
			var m int
			fmt.Scan(&m)
			num[i] = m
		}
		nums = append(nums, num)
	}

	return
}
func doNums(nums []int) {
	//fmt.Printf("Debug:doNums,nums:%v\n", nums)
	space := make([]int, 500)
	for _, n := range nums {
		//fmt.Printf("Debug:nums,n:%v\n", n)
		space[n] = 1
	}
	for i, n := range space {
		if n == 0 {
			continue
		} else {
			fmt.Println(i)
		}
	}
}

func main() {
	for _, nums := range GetInputNumbers() {
		doNums(nums)
	}
}
