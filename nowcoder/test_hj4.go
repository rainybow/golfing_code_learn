package main

import (
	"fmt"
	"os"
)

func fill(s string) string {
	for len(s) < 8 {
		s += "0"
	}
	return s
}
func main() {
	for {
		s := ""
		if n, err := fmt.Scanf("%s", &s); err != nil || n == 0 {
			os.Exit(0)
		}
		for len(s) >= 8 {
			fmt.Println(s[:8])
			s = s[8:]
		}
		if len(s) > 0 {
			fmt.Println(fill(s))
		}

	}
}
