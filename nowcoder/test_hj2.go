package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func get_input() (s string, s1 string) {
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	s = input.Text()
	s = strings.ToLower(s)
	input.Scan()
	s1 = input.Text()
	s1 = strings.ToLower(s1)

	return
}
func main() {
	s, c := get_input()
	fmt.Println(strings.Count(s, c))
}
