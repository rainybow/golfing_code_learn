package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {

	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	s := input.Text()
	fields := strings.Fields(s)
	if len(fields) == 0 {
		fmt.Println(0)
	} else {
		fmt.Println(len(fields[len(fields)-1]))
	}

}
