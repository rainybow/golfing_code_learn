package main

import (
	"fmt"
	"strings"
)

func detect_char(s string) bool {
	if strings.ContainsRune(s, ' ') ||
		strings.ContainsRune(s, '\n') {
		return false
	}
	nums := make([]int, 4)
	for _, c := range s {
		if '0' <= c && '9' >= c {
			nums[0] = 1
		} else if 'a' <= c && 'z' >= c {
			nums[1] = 1
		} else if 'A' <= c && 'Z' >= c {
			nums[2] = 1
		} else {
			nums[3] = 1
		}
		if nums[0]+nums[1]+nums[2]+nums[3] >= 3 {
			return true
		}
	}
	return false
}

func detect_sub(s string) bool {
	l := len(s)
	for i := 0; i < l-3; i++ {
		right := i
		j := i + 3
		for j < l {
			if s[j] == s[right] {
				right++
			} else {
				if right-i > 2 {
					return false
				} else {
					right = i
				}
			}
			j++
		}
		if right-i > 2 {
			return false
		}
	}
	return true
}
func main() {
	strs := []string{}

	for {
		var s string
		fmt.Scanln(&s)
		if len(s) == 0 {
			break
		}
		strs = append(strs, s)

	}
	for _, s := range strs {
		if len(s) > 8 && detect_char(s) && detect_sub(s) {
			fmt.Println("OK")
		} else {
			fmt.Println("NG")
		}
	}

}
