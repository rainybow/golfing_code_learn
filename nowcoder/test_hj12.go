/*
描述
接受一个只包含小写字母的字符串，然后输出该字符串反转后的字符串。（字符串长度不超过1000）

输入描述：
输入一行，为一个只包含小写字母的字符串。

输出描述：
输出该字符串反转后的字符串。

示例1
输入：
abcd
复制
输出：
dcba
*/
package main

import (
	"fmt"
)

func main() {
	var s string
	fmt.Scan(&s)
	for n := len(s) - 1; n > -1; n-- {
		fmt.Printf("%s", string(s[n]))
	}
}
