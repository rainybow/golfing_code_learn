/*
描述
请解析IP地址和对应的掩码，进行分类识别。要求按照A/B/C/D/E类地址归类，不合法的地址和掩码单独归类。

所有的IP地址划分为 A,B,C,D,E五类

A类地址1.0.0.0~126.255.255.255;

B类地址128.0.0.0~191.255.255.255;

C类地址192.0.0.0~223.255.255.255;

D类地址224.0.0.0~239.255.255.255；

E类地址240.0.0.0~255.255.255.255


私网IP范围是：

10.0.0.0-10.255.255.255

172.16.0.0-172.31.255.255

192.168.0.0-192.168.255.255


子网掩码为二进制下前面是连续的1，然后全是0。（例如：255.255.255.32就是一个非法的掩码）
（注意二进制下全是1或者全是0均为非法子网掩码）

注意：
1. 类似于【0.*.*.*】和【127.*.*.*】的IP地址不属于上述输入的任意一类，也不属于不合法ip地址，计数时请忽略
2. 私有IP地址和A,B,C,D,E类地址是不冲突的

输入描述：
多行字符串。每行一个IP地址和掩码，用~隔开。

输出描述：
统计A、B、C、D、E、错误IP地址或错误掩码、私有IP的个数，之间以空格隔开。

示例1
输入：
10.70.44.68~255.254.255.0
1.0.0.1~255.0.0.0
192.168.0.2~255.255.255.0
19..0.~255.255.255.0
复制
输出：
1 0 1 0 0 2 1
复制
说明：
10.70.44.68~255.254.255.0的子网掩码非法，19..0.~255.255.255.0的IP地址非法，所以错误IP地址或错误掩码的计数为2；
1.0.0.1~255.0.0.0是无误的A类地址；
192.168.0.2~255.255.255.0是无误的C类地址且是私有IP；
所以最终的结果为1 0 1 0 0 2 1
示例2
输入：
0.201.56.50~255.255.111.255
127.201.56.50~255.255.111.255
复制
输出：
0 0 0 0 0 0 0
复制
说明：
类似于【0.*.*.*】和【127.*.*.*】的IP地址不属于上述输入的任意一类，也不属于不合法ip地址，计数时请忽略
*/
package main

import (
	"fmt"
	"net"
	"strings"
)

var (
	a1   = ip2Int(parseIP("1.0.0.0"))
	a2   = ip2Int(parseIP("126.255.255.255"))
	b1   = ip2Int(parseIP("128.0.0.0"))
	b2   = ip2Int(parseIP("191.255.255.255"))
	c1   = ip2Int(parseIP("192.0.0.0"))
	c2   = ip2Int(parseIP("223.255.255.255"))
	d1   = ip2Int(parseIP("224.0.0.0"))
	d2   = ip2Int(parseIP("239.255.255.255"))
	e1   = ip2Int(parseIP("240.0.0.0"))
	e2   = ip2Int(parseIP("255.255.255.255"))
	zero = parseIP("0.0.0.0")
	ones = parseIP("255.255.255.255")
)

func ip2Int(ip net.IP) int {
	ret := 0
	for i := 0; i < 4; i++ {
		ret = ret*256 + int(ip[i])
	}
	return ret
}
func parseIP(s string) net.IP {
	if ip := net.ParseIP(s); ip != nil {
		return ip.To4()
	}
	return nil
}
func isVaidMask(ip net.IP) bool {
	if ip.Equal(zero) || ip.Equal(ones) {
		return false
	}
	for i := 0; i < 4; i++ {
		if i < 3 && ip[i] < ip[i+1] {
			return false
		}

		switch ip[i] {
		case 255, 0, 254, 252, 248, 240, 224, 192, 128:
			continue
		default:
			return false
		}
	}

	return true
}
func IsPrivate(ip net.IP) bool {
	/*
			私网IP范围是：

		10.0.0.0-10.255.255.255

		172.16.0.0-172.31.255.255

		192.168.0.0-192.168.255.255
	*/
	return ip[0] == 10 ||
		(ip[0] == 172 && ip[1] >= 16 && ip[1] <= 31) ||
		(ip[0] == 192 && ip[1] == 168)
}
func test() {
	ip := parseIP("192.168.1.10")
	fmt.Println(ip)
	for i, b := range ip {
		fmt.Printf("ip %d:%d\n", i, b)
	}
	mask := parseIP("255.255.255.42")
	fmt.Println(mask, isVaidMask(mask), mask.String())
	for i, b := range mask {
		fmt.Printf("mask %d:%d\n", i, b)
	}
}
func main() {
	test()
	array := make([]int, 7)
	for {
		var s string
		if _, err := fmt.Scanf("%s", &s); err != nil {
			//fmt.Printf("Error:%v\n", err)
			break
		}
		sl := strings.Split(s, "~")
		if len(sl) != 2 {
			array[5]++
			continue
		}
		ip := parseIP(sl[0])
		mask := parseIP(sl[1])
		if ip == nil || mask == nil || !isVaidMask(mask) {
			array[5]++
			continue
		}
		if IsPrivate(ip) {
			array[6]++
		}
		ipint := ip2Int(ip)
		if ipint >= a1 && ipint <= a2 {
			array[0]++
		} else if ipint >= b1 && ipint <= b2 {
			array[1]++
		} else if ipint >= c1 && ipint <= c2 {
			array[2]++
		} else if ipint >= d1 && ipint <= d2 {
			array[3]++
		} else if ipint >= e1 && ipint <= e2 {
			array[4]++
		}
	}

	fmt.Printf("%d %d %d %d %d %d %d", array[0], array[1], array[2],
		array[3], array[4], array[5], array[6])
}
