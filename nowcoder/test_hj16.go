/*
描述
王强今天很开心，公司发给N元的年终奖。王强决定把年终奖用于购物，他把想买的物品分为两类：主件与附件，附件是从属于某个主件的，下表就是一些主件与附件的例子：

主件	附件
电脑	打印机，扫描仪
书柜	图书
书桌	台灯，文具
工作椅	无

如果要买归类为附件的物品，必须先买该附件所属的主件。每个主件可以有 0 个、 1 个或 2 个附件。附件不再有从属于自己的附件。王强想买的东西很多，为了不超出预算，他把每件物品规定了一个重要度，分为 5 等：用整数 1 ~ 5 表示，第 5 等最重要。他还从因特网上查到了每件物品的价格（都是 10 元的整数倍）。他希望在不超过 N 元（可以等于 N 元）的前提下，使每件物品的价格与重要度的乘积的总和最大。
    设第 j 件物品的价格为 v[j] ，重要度为 w[j] ，共选中了 k 件物品，编号依次为 j 1 ， j 2 ，……， j k ，则所求的总和为：
v[j 1 ]*w[j 1 ]+v[j 2 ]*w[j 2 ]+ … +v[j k ]*w[j k ] 。（其中 * 为乘号）
    请你帮助王强设计一个满足要求的购物单。



输入描述：
输入的第 1 行，为两个正整数，用一个空格隔开：N m

（其中 N （ <32000 ）表示总钱数， m （ <60 ）为希望购买物品的个数。）


从第 2 行到第 m+1 行，第 j 行给出了编号为 j-1 的物品的基本数据，每行有 3 个非负整数 v p q


（其中 v 表示该物品的价格（ v<10000 ）， p 表示该物品的重要度（ 1 ~ 5 ）， q 表示该物品是主件还是附件。如果 q=0 ，表示该物品为主件，如果 q>0 ，表示该物品为附件， q 是所属主件的编号）




输出描述：
 输出文件只有一个正整数，为不超过总钱数的物品的价格与重要度乘积的总和的最大值（ <200000 ）。
示例1
输入：
1000 5
800 2 0
400 5 1
300 5 1
400 3 0
500 2 0
复制
输出：
2200
*/
package main

import (
	"fmt"
)

type Value struct {
	Price int
	Value int
	//	Ratio float64
}
type Item struct {
	ID       int
	Price    int
	Weight   int
	Parent   int
	MaxPrice int
	MinPrice int
	Sub1     Value
	Sub2     Value
	Values   []Value
}

var (
	itemMaps = map[int]*Item{}
)

func get_values(total int, itemMap map[int]*Item) int {
	total = total / 10
	all_values := make([][]int, 0)
	itemArray := []*Item{}
	for _, item := range itemMap {
		itemArray = append(itemArray, item)
		values := make([]int, total+1)
		all_values = append(all_values, values)
	}
	values := make([]int, total+1)
	all_values = append(all_values, values)
	n := len(itemArray)
	for i := 1; i < n+1; i++ {
		for j := total; j > -1; j-- {
			item := itemArray[i-1]
			//fmt.Printf("i=%d,j=%d\n", i, j)
			if j >= item.Values[0].Price {
				all_values[i][j] = max(all_values[i-1][j],
					all_values[i-1][j-item.Values[0].Price]+item.Values[0].Value)
				if item.Sub1.Price > 0 && j >= item.Values[1].Price {
					all_values[i][j] = max(all_values[i][j], all_values[i-1][j],
						all_values[i-1][j-item.Values[1].Price]+item.Values[1].Value)
				}
				if item.Sub2.Price > 0 && j >= item.Values[2].Price {
					all_values[i][j] = max(all_values[i][j], all_values[i-1][j],
						all_values[i-1][j-item.Values[2].Price]+item.Values[2].Value)
					if j >= item.Values[3].Price {
						all_values[i][j] = max(all_values[i][j], all_values[i-1][j],
							all_values[i-1][j-item.Values[3].Price]+item.Values[3].Value)

					}
				}
			} else {
				all_values[i][j] = all_values[i-1][j]
			}
		}
	}
	return all_values[n][total] * 10

}

func max(i int, other ...int) int {
	m := i
	for _, j := range other {
		if j > m {
			m = j
		}
	}
	return m
}
func get_input() (map[int]*Item, int) {
	var money, m int
	fmt.Scan(&money, &m)
	subs := []*Item{}
	for i := 0; i < m; i++ {
		var price, weight, parent int
		fmt.Scanf("%d %d %d", &price, &weight, &parent)
		price = price / 10
		item := &Item{i + 1, price, weight, parent, price, price, Value{}, Value{}, []Value{}}
		if parent == 0 {
			item.Values = append(item.Values, Value{price, price * weight})
			itemMaps[i+1] = item
		} else {
			subs = append(subs, item)
		}
	}
	for _, sub := range subs {
		item_parent := itemMaps[sub.Parent]
		if item_parent.Sub1.Value == 0 {
			item_parent.Sub1 = Value{sub.Price, sub.Price * sub.Weight}
			item_parent.MaxPrice += sub.Price
			item_parent.Values = append(item_parent.Values, Value{
				item_parent.Sub1.Price + item_parent.Values[0].Price,
				item_parent.Sub1.Value + item_parent.Values[0].Value,
			})
		} else {
			item_parent.Sub2 = Value{sub.Price, sub.Price * sub.Weight}
			item_parent.MaxPrice += sub.Price
			item_parent.Values = append(item_parent.Values, Value{
				item_parent.Sub2.Price + item_parent.Values[0].Price,
				item_parent.Sub2.Value + item_parent.Values[0].Value,
			})
			item_parent.Values = append(item_parent.Values, Value{
				item_parent.Sub2.Price + item_parent.Sub1.Price + item_parent.Values[0].Price,
				item_parent.Sub2.Value + item_parent.Sub1.Value + item_parent.Values[0].Value,
			})
		}
	}
	return itemMaps, money
}

func testdata() (map[int]*Item, int) {
	money := 50
	itemMaps := map[int]*Item{}
	itemMaps[5] = &Item{
		ID:     5,
		Price:  1,
		Weight: 1,
		Sub1:   Value{2, 6},
		Sub2:   Value{2, 6},
		Values: []Value{
			Value{1, 1},
			Value{3, 7},
			Value{3, 7},
			Value{5, 13},
		},
	}
	itemMaps[4] = &Item{
		ID:     4,
		Price:  1,
		Weight: 2,
		Sub1:   Value{},
		Sub2:   Value{},
		Values: []Value{
			Value{1, 2},
		},
	}
	itemMaps[3] = &Item{
		ID:     4,
		Price:  1,
		Weight: 3,
		Sub1:   Value{},
		Sub2:   Value{},
		Values: []Value{
			Value{1, 3},
		},
	}
	return itemMaps, money
}
func main() {
	//itemMaps, money := get_input()
	itemMaps, money := testdata()
	fmt.Println(get_values(money, itemMaps))
}
