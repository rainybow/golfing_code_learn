/*
注意本题有多组输入
输入描述：
输入一个英文语句，每个单词用空格隔开。保证输入只包含空格和字母。

输出描述：
得到逆序的句子

示例1
输入：
I am a boy
复制
输出：
boy a am I
复制
示例2
输入：
nowcoder
复制
输出：
nowcoder
*/

package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func main() {
	for {
		input := bufio.NewScanner(os.Stdin)
		if !input.Scan() {
			return
		}
		text := input.Text()
		fileds := strings.Fields(text)
		n := len(fileds)
		for i := 0; i < n/2; i++ {
			fileds[i], fileds[n-1-i] = fileds[n-1-i], fileds[i]
		}
		fmt.Println(strings.Join(fileds, " "))
	}
}
