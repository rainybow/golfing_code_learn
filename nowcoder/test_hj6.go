/*
描述
功能:输入一个正整数，按照从小到大的顺序输出它的所有质因子（重复的也要列举）（如180的质因子为2 2 3 3 5 ）


数据范围：
输入描述：
输入一个整数

输出描述：
按照从小到大的顺序输出它的所有质数的因子，以空格隔开。最后一个数后面也要有空格。

示例1
输入：
180
复制
输出：
2 2 3 3 5
*/
package main

import (
	"fmt"
)

func main() {
	var n int
	fmt.Scan(&n)
	if n == 1 {
		fmt.Println("1 ")
	}
	for i := 2; i*i <= n; i++ {
		for n%i == 0 {
			fmt.Printf("%d ", i)
			n = n / i
		}
	}
	if n > 1 {
		fmt.Printf("%d ", n)
	}
}
