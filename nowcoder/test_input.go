package main

import (
	"bufio"
	"fmt"
	"os"
	"strings"
)

func inputKnownNumberArgsTest1() {
	// 先输入n
	// 再输入n个数    这个n个数可以用空格或者换行隔开
	var n int
	fmt.Scan(&n)
	fmt.Println(n)
	nums := []int{}
	for ; n > 0; n-- {
		var i int
		fmt.Scan(&i)
		nums = append(nums, i)
	}
	fmt.Println(nums)
}
func inputKnownNumberArgsTest2() {
	// 先输入n
	// 再输入n字符串    这个n个数可以用空格或者换行隔开
	var n int
	fmt.Scan(&n)
	fmt.Println(n)
	nums := []string{}
	for ; n > 0; n-- {
		var i string
		fmt.Scan(&i)
		nums = append(nums, i)
	}
	fmt.Println(nums)
}
func inputUnknownNumberArgsTest1() {
	// 在一行中输入不定个数不定长度的数，
	nums := []int{}
	for {
		var i int
		if _, err := fmt.Scanf("%d", &i); err != nil {
			break
		}
		nums = append(nums, i)
	}
	fmt.Println(nums)
}
func inputUnknownNumberArgsTest2() {
	// 在一行中输入不定个数不定长度的字符串，每个字符串用空格隔开  不考虑转义和引号
	nums := []string{}
	input := bufio.NewScanner(os.Stdin)
	input.Scan()
	text := input.Text()
	nums = strings.Fields(text)
	fmt.Println(nums)
}
func main() {
	//inputKnownNumberArgsTest1()
	//inputKnownNumberArgsTest2()
	//inputUnknownNumberArgsTest1()
	//inputUnknownNumberArgsTest2()
	for i := 0; i < 5; i++ {
		fmt.Println(i, 1<<i)
	}
}
