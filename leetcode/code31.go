/*
31. 下一个排列
整数数组的一个 排列  就是将其所有成员以序列或线性顺序排列。

例如，arr = [1,2,3] ，以下这些都可以视作 arr 的排列：[1,2,3]、[1,3,2]、[3,1,2]、[2,3,1] 。
整数数组的 下一个排列 是指其整数的下一个字典序更大的排列。更正式地，如果数组的所有排列根据其字典顺序从小到大排列在一个容器中，那么数组的 下一个排列 就是在这个有序容器中排在它后面的那个排列。如果不存在下一个更大的排列，那么这个数组必须重排为字典序最小的排列（即，其元素按升序排列）。

例如，arr = [1,2,3] 的下一个排列是 [1,3,2] 。
类似地，arr = [2,3,1] 的下一个排列是 [3,1,2] 。
而 arr = [3,2,1] 的下一个排列是 [1,2,3] ，因为 [3,2,1] 不存在一个字典序更大的排列。
给你一个整数数组 nums ，找出 nums 的下一个排列。

必须 原地 修改，只允许使用额外常数空间。
*/
package main

import "fmt"

func nextPermutation(nums []int) {
	n := len(nums)
	if n < 2 {
		return
	}
	if n == 2 {
		nums[0], nums[1] = nums[1], nums[0]
		return
	}
	left := 0
	for i := n - 1; i > 0; i-- {
		if nums[i] > nums[i-1] {
			// 找到i了
			k := i

			for j := n - 1; j > k; j-- {
				if nums[j] > nums[i-1] {
					k = j
					break
				}
			}
			//fmt.Printf("i=%d,k=%d\n",i,k)
			nums[i-1], nums[k] = nums[k], nums[i-1]
			left = i
			break
		}
	}
	for i, j := left, n-1; i < j; {
		nums[i], nums[j] = nums[j], nums[i]
		i++
		j--
	}
}
func main() {
	inputs := [][]int{{1, 2, 3}, {3, 2, 1}, {1, 1, 5}}
	for i, nums := range inputs {
		fmt.Printf("第%d组测试，输入%v", i, nums)
		nextPermutation(nums)
		fmt.Printf(",nextPermutation 计算后输出%v\n", nums)
	}
}
